using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;

namespace Advent.PuzzleThree
{
    
    public struct Line
    {
        public int StartX { get; set; }
        public int EndX { get; set; }
        
        public int StartY { get; set; }

        public int EndY { get; set; }
    }

    public class WireIntersection
    {
        public int Distance { get; set; }
        public int WireLength { get; set; }
    }


    public static class Solver
    {
        private static List<string> Data = new List<string>();

// 399
        public static int SolveA()
        {
            Load();
            var dataA = Data.First().Split(',', StringSplitOptions.RemoveEmptyEntries);
            var dataB = Data.Last().Split(',', StringSplitOptions.RemoveEmptyEntries);

            var linesA = ParseWire(dataA);
            var linesB = ParseWire(dataB);
            var distances = new List<int>();
            foreach (var lineA in linesA)
            {
                foreach (var lineB in linesB)
                {
                    var intersect = FindIntersection(lineA, lineB);
                    if (intersect.HasValue)
                    {
                        distances.Add(ManhattanDistance(intersect.Value.X, intersect.Value.Y));
                    }
                }
            }

            return distances.Min(i => i);
            
        }
        public static int SolveB()
        {
            Load();
            var dataA = Data.First().Split(',', StringSplitOptions.RemoveEmptyEntries);
            var dataB = Data.Last().Split(',', StringSplitOptions.RemoveEmptyEntries);

            var linesA = ParseWire(dataA);
            var linesB = ParseWire(dataB);
            var distances = new List<WireIntersection>();
            
            var lengthWireA = 0;
            foreach (var lineA in linesA)
            {
                var lengthWireB = 0;
                foreach (var lineB in linesB)
                {
                    
                    var intersect = FindIntersection(lineA, lineB);
                    if (intersect.HasValue)
                    {
                        var lengthIntersectA = Math.Abs(intersect.Value.X - lineA.StartX) + Math.Abs(intersect.Value.Y - lineA.StartY);
                        var lengthIntersectB = Math.Abs(intersect.Value.X - lineB.StartX) + Math.Abs(intersect.Value.Y - lineB.StartY);
                        var distance = ManhattanDistance(intersect.Value.X, intersect.Value.Y);
                        distances.Add(new WireIntersection
                        {
                            Distance = distance,
                            WireLength = lengthWireA + lengthIntersectA + lengthWireB + lengthIntersectB
                        });
                        
                    }
                    
                    var lengthLineB = Math.Abs(lineB.EndX - lineB.StartX) + Math.Abs(lineB.EndY - lineB.StartY);

                    lengthWireB += lengthLineB;
                }
                var lengthLineA = Math.Abs(lineA.EndX - lineA.StartX) + Math.Abs(lineA.EndY - lineA.StartY);
                lengthWireA += lengthLineA;
            }

            return distances.OrderBy(i => i.WireLength).First().WireLength;
        }

        private static List<Line> ParseWire(string[] wireData)
        {
            var lines = new List<Line>();
            foreach (var data in wireData)
            {
                var instruction = data[0];
                var movement = int.Parse(data.Substring(1, data.Length - 1));
                var lastLine = lines.LastOrDefault();
                var line = NextLine(lastLine, instruction, movement);
                lines.Add(line);
            }

            return lines;
        }

        private static Line NextLine(Line previous, char instruction, int movement)
        {
            var x = previous.EndX;
            var y = previous.EndY;

            switch (instruction)
            {
                case 'R':
                    x += movement;
                    break;
                case 'L':
                    x -= movement;
                    break;
                case 'U':
                    y += movement;
                    break;
                case 'D':
                    y -= movement;
                    break;
            }

            return new Line
            {
                StartX = previous.EndX,
                EndX = x,
                StartY = previous.EndY,
                EndY = y
            };
        }

        public static Point? FindIntersection(Line lineA, Line lineB, double tolerance = 0.001)
        {
            var aStartX = lineA.StartX;
            var aStartY = lineA.StartY;
            var aEndX = lineA.EndX;
            var aEndY = lineA.EndY;

            var bStartX = lineB.StartX;
            var bStartY = lineB.StartY;
            var bEndX = lineB.EndX;
            var bEndY = lineB.EndY;

            if (Math.Abs(aStartX - aEndX) < tolerance && Math.Abs(bStartX - bEndX) < tolerance)
            {
                return null;
            }

            //equations of the form y=c (two horizontal lines)
            if (Math.Abs(aStartY - aEndY) < tolerance && Math.Abs(bStartY - bEndY) < tolerance)
            {
                return null;
            }

            int x, y;

            //lineA is vertical x1 = x2
            //slope will be infinity
            //so lets derive another solution
            if (Math.Abs(aStartX - aEndX) < tolerance)
            {
                //compute slope of line 2 (m2) and c2
                var m2 = (bEndY - bStartY) / (bEndX - bStartX);
                var c2 = -m2 * bStartX + bStartY;

                //equation of vertical line is x = c
                //if line 1 and 2 intersect then x1=c1=x
                //substitute x=x1 in (4) => -m2x1 + y = c2
                // => y = c2 + m2x1 
                x = aStartX;
                y = c2 + m2 * aStartX;
            }
            //lineB is vertical x3 = x4
            //slope will be infinity
            //so lets derive another solution
            else if (Math.Abs(bStartX - bEndX) < tolerance)
            {
                //compute slope of line 1 (m1) and c2
                var m1 = (aEndY - aStartY) / (aEndX - aStartX);
                var c1 = -m1 * aStartX + aStartY;

                //equation of vertical line is x = c
                //if line 1 and 2 intersect then x3=c3=x
                //substitute x=x3 in (3) => -m1x3 + y = c1
                // => y = c1 + m1x3 
                x = bStartX;
                y = c1 + m1 * bStartX;
            }
            //lineA & lineB are not vertical 
            //(could be horizontal we can handle it with slope = 0)
            else
            {
                //compute slope of line 1 (m1) and c2
                var m1 = (aEndY - aStartY) / (aEndX - aStartX);
                var c1 = -m1 * aStartX + aStartY;

                //compute slope of line 2 (m2) and c2
                var m2 = (bEndY - bStartY) / (bEndX - bStartX);
                var c2 = -m2 * bStartX + bStartY;

                //solving equations (3) & (4) => x = (c1-c2)/(m2-m1)
                //plugging x value in equation (4) => y = c2 + m2 * x
                x = (c1 - c2) / (m2 - m1);
                y = c2 + m2 * x;

                //verify by plugging intersection point (x, y)
                //in original equations (1) & (2) to see if they intersect
                //otherwise x,y values will not be finite and will fail this check
                if (!(Math.Abs(-m1 * x + y - c1) < tolerance
                      && Math.Abs(-m2 * x + y - c2) < tolerance))
                {
                    return null;
                }
            }

            //x,y can intersect outside the line segment since line is infinitely long
            //so finally check if x, y is within both the line segments
            if (IsInsideLine(lineA, x, y) && IsInsideLine(lineB, x, y))
            {
                return new Point(x, y);
            }
            
            return null;
        }

        // Returns true if given point(x,y) is inside the given line segment
        private static bool IsInsideLine(Line line, int x, int y)
        {
            return (x >= line.StartX && x <= line.EndX
                    || x >= line.EndX && x <= line.StartX)
                   && (y >= line.StartY && y <= line.EndY
                       || y >= line.EndY && y <= line.StartY);
        }

        private static int ManhattanDistance(int x, int y)
        {
            return Math.Abs(0 - x) + Math.Abs(0 - y);
        }

        private static void Load()
        {
            if (System.IO.File.Exists("PuzzleThree/Data1.txt"))
            {
                using (var file = new System.IO.StreamReader("PuzzleThree/Data1.txt"))
                {
                    string line;
                    while ((line = file.ReadLine()) != null)
                    {
                        Data.Add(line);
                    }

                }
            }
        }
    }
}