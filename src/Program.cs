﻿using System;

namespace Advent
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(PuzzleOne.Solver.SolvePart1());
            //Console.WriteLine(PuzzleOne.Solver.SolvePart2());
            //Console.WriteLine(PuzzleTwo.Solver.Calc());
            //Console.WriteLine(PuzzleTwo.Solver.Part2(19690720));
            //Console.WriteLine(PuzzleThree.Solver.SolveA());
            //Console.WriteLine(PuzzleFour.Solver.SolveA());
            Console.WriteLine(PuzzleFour.Solver.SolveB());
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}