using System;
using System.ComponentModel.DataAnnotations;

namespace Advent.PuzzleOne
{
    public class Solver
    {
        public static int SolvePart1()
        {
            var total = 0;
            using (var file = new System.IO.StreamReader("PuzzleOne/Data.txt"))
            {
                string line;
                while((line = file.ReadLine()) != null)  
                {
                    if (int.TryParse(line, out var mass))
                    {
                        total += Fuel(mass);
                    }  
                }  
            }

            return total;
        }
        
        
        public static int SolvePart2()
        {
            var total = 0;
            using (var file = new System.IO.StreamReader("PuzzleOne/Data2.txt"))
            {
                string line;
                while((line = file.ReadLine()) != null)  
                {
                    if (int.TryParse(line, out var mass))
                    {
                        var starFuel = Fuel(mass);
                        var fuelRequirement = FuelRequirement(starFuel);
                        total += (starFuel + fuelRequirement);
                    }  
                }  
            }

            return total;
        }

        private static int FuelRequirement(int starFuel)
        {
            var total = 0;
            while (true)
            {
                var load = Fuel(starFuel);
                if (load > 0)
                {
                    total += load;
                    starFuel = load;
                }
                else
                {
                    break;
                }
            }
            return total;
        }

        private static int Fuel(int mass)
        {
            return Convert.ToInt32(Math.Floor((double) (mass / 3))) - 2;
        }
    }
}