using System;
using System.Collections.Generic;
using System.Linq;

namespace Advent.PuzzleTwo
{
    public static class Solver
    {
        public static List<int> IntCode;

        public static int Part2(int target)
        {
            for (var noun = 1; noun < 99; noun++)
            {
                for (var verb = 1; verb < 99; verb++)
                {
                    var result = Calc(0, noun, verb);
                    if (result == target)
                    {
                        return (100 * noun + verb);
                    }
                }
            }

            return 0;
        }

        public static int Calc(int position = 0, int noun = 12, int verb = 0)
        {
            if (position == 0)
            {
                Load(noun, verb);
            }
            if (IntCode.Count < position + 3)
            {
                return IntCode[0];
            }
            
            var opCode = IntCode[position];
            var x = IntCode[position + 1];
            var y = IntCode[position + 2];
            var outputPosition = IntCode[position + 3];

            var result = Process(opCode, new[] {x, y, outputPosition});
            
            return result ? Calc(position + 4) : IntCode[0];
        }
        
        private static bool Process(int opCode, int[] args)
        {
            if (opCode == 99)
            {
                return false;
            }

            var valueX = IntCode[args[0]];
            var valueY = IntCode[args[1]];
            var outputPosition = args[2];
                
            if (opCode == 1)
            {
                IntCode[outputPosition] = valueX + valueY;;
            } 
                
            if (opCode == 2)
            {
                IntCode[outputPosition] = valueX * valueY;;
            }
            return true;
        }
        
        private static void Load(int noun, int verb)
        {
            if (System.IO.File.Exists("PuzzleTwo/Data1.txt"))
            {
                string line = null;
                using (var file = new System.IO.StreamReader("PuzzleTwo/Data1.txt"))
                {
                    line = file.ReadLine();
                }

                IntCode = new List<int>(
                    line
                        .Split(',', StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse));
                
                Reset(noun, verb);
            }
        }

        private static void Reset(int noun, int verb)
        {
            IntCode[1] = noun;
            IntCode[2] = verb;
        }
    }
}