using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.ConstrainedExecution;
using System.Text.RegularExpressions;
using System.Xml.Schema;

namespace Advent.PuzzleFour
{
    public static class Solver
    {
        static Regex DoubleRegEx = new Regex(@"(.)\1{1,}");
        public static int SolveA()
        {
            var rangeStart = 254032; //
            var rangeEnd = 789860; //
            var numbers = Enumerable.Range(rangeStart, rangeEnd-rangeStart)
                .Where(i => Passes(i))
                .ToList();

            return numbers.Count;
        }
        
        public static int SolveB()
        {
            var rangeStart = 254032; //
            var rangeEnd = 789860; //
            var numbers = Enumerable.Range(rangeStart, rangeEnd-rangeStart)
                .Where(i => PassesB(i))
                .ToList();

            return numbers.Count;
        }
        
        private static bool PassesB(int i)
        {
            
            var s = $"{i}";
            var ints = s.Select(n => int.Parse(n.ToString())).ToList();
            if (s.Length != 6)
            {
                return false;
            }
            
            if (ints[5] < ints[4] || ints[4] < ints[3] || ints[3] < ints[2] || ints[2] < ints[1] || ints[1] < ints[0])
            {
                return false;
            }

            var currentCount = 1;

            for (var l = 1; l < s.Count(); l++)
            {
                if (s[l-1] == s[l])
                {
                    currentCount += 1;
                }
                else
                {
                    if (currentCount == 2)
                    {
                        return true;
                    }
                    currentCount = 1;
                }
            }

            return currentCount == 2;
        }

        private static bool Passes(int i)
        {
            var s = $"{i}";
            var ints = s.Select(n => int.Parse(n.ToString())).ToList();
            if (s.Length != 6)
            {
                return false;
            }

            if (!DoubleRegEx.Match(s).Success)
            {
                return false;
            }

            if (ints[5] < ints[4] || ints[4] < ints[3] || ints[3] < ints[2] || ints[2] < ints[1] || ints[1] < ints[0])
            {
                return false;
            }

            return true;
        }
    }
}